import com.javaPrac.JavaBasic.GeeksForGeeks.LinkedList.LinkedList;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class LinkedListLengthTest {

    @Mock
    static LinkedList list;

    @Before
    public void setup(){
        list.add(5);
        list.add(2);
        list.add(3);
        list.add(8);
        list.add(9);
    }
    @Test
    public void findSize(){
        Assert.assertEquals(5,list.size(LinkedList.getFirst()));
    }

    /*@Test
    public void reverse(){
        Assert.assertEquals(5,LinkedList.getFirst().getData());
        list.reverse(LinkedList.getFirst());
        Assert.assertEquals(9,LinkedList.getFirst().getData());
    }*/
}
