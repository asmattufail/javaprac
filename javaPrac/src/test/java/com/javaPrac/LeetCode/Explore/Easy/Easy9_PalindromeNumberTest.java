package com.javaPrac.LeetCode.Explore.Easy;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class Easy9_PalindromeNumberTest {

    @Test
    void isPalindrome() {
        Assert.assertEquals(true, Easy9_PalindromeNumber.isPalindrome(121));
        Assert.assertEquals(false, Easy9_PalindromeNumber.isPalindrome(-121));
        Assert.assertEquals(false, Easy9_PalindromeNumber.isPalindrome(10));
        Assert.assertEquals(false, Easy9_PalindromeNumber.isPalindrome(-101));
    }
}