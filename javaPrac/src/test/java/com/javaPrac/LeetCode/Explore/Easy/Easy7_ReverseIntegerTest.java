package com.javaPrac.LeetCode.Explore.Easy;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class Easy7_ReverseIntegerTest {

    @Test
    void reverse() {
        Assert.assertEquals(321, Easy7_ReverseInteger.reverse(123));
        Assert.assertEquals(-321, Easy7_ReverseInteger.reverse(-123));
        Assert.assertEquals(21, Easy7_ReverseInteger.reverse(120));
        Assert.assertEquals(0, Easy7_ReverseInteger.reverse(1534236469));

    }
}