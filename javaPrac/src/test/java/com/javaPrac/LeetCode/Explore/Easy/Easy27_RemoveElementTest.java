package com.javaPrac.LeetCode.Explore.Easy;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Easy27_RemoveElementTest {

    @Test
    void removeElement() {
        Assert.assertEquals(2, Easy27_RemoveElement.removeElement(new int[]{3,2,2,3}, 3));
    }
}