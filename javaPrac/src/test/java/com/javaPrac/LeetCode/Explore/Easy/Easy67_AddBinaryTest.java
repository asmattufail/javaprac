package com.javaPrac.LeetCode.Explore.Easy;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class Easy67_AddBinaryTest {

    @Test
    void addBinary() {
        //Assert.assertEquals("100", Easy67_AddBinary.addBinary("11","1"));
        Assert.assertEquals("1", Easy67_AddBinary.addBinary("0","1"));
        //Assert.assertEquals("0", Easy67_AddBinary.addBinary("10100000100100110110010000010101111011011001101110111111111101000000101111001110001111100001101"
        //"110101001011101110001111100110001010100001101011101010000011011011001011101111001100000011011110011"));
    }
}