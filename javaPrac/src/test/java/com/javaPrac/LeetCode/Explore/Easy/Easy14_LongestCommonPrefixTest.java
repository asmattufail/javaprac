package com.javaPrac.LeetCode.Explore.Easy;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class Easy14_LongestCommonPrefixTest {

    @Test
    void longestCommonPrefix() {
        Assert.assertEquals("fl", Easy14_LongestCommonPrefix.longestCommonPrefix(new String[]{"flower","flow","flight"}));
        Assert.assertEquals("", Easy14_LongestCommonPrefix.longestCommonPrefix(new String[]{"dog","racecar","car"}));
    }
}