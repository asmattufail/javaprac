package com.javaPrac.LeetCode.Explore.Easy;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class Easy13_RomanToIntTest {

    @Test
    void romanToInt() {
        Assert.assertEquals(1994, Easy27_RemoveElement.Easy13_RomanToInt.romanToInt("MCMXCIV"));
        Assert.assertEquals(3, Easy27_RemoveElement.Easy13_RomanToInt.romanToInt("III"));
        Assert.assertEquals(4, Easy27_RemoveElement.Easy13_RomanToInt.romanToInt("IV"));
        Assert.assertEquals(9, Easy27_RemoveElement.Easy13_RomanToInt.romanToInt("IX"));
        Assert.assertEquals(58, Easy27_RemoveElement.Easy13_RomanToInt.romanToInt("LVIII"));
    }
}