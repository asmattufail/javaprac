package com.javaPrac.LeetCode.Explore.Easy;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Easy58_LengthOfLastWordTest {

    @Test
    void lengthOfLastWord() {
        Assert.assertEquals(1,Easy58_LengthOfLastWord.lengthOfLastWord("a"));
        Assert.assertEquals(4,Easy58_LengthOfLastWord.lengthOfLastWord("   fly me   to   the moon  "));
        Assert.assertEquals(5,Easy58_LengthOfLastWord.lengthOfLastWord("Hello World"));
        Assert.assertEquals(6,Easy58_LengthOfLastWord.lengthOfLastWord("luffy is still joyboy"));
    }
}