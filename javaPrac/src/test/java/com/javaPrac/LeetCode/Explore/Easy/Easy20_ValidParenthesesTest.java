package com.javaPrac.LeetCode.Explore.Easy;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class Easy20_ValidParenthesesTest {

    @Test
    void isValid() {
        Assert.assertEquals(true, Easy20_ValidParentheses.isValid("()"));
        Assert.assertEquals(true, Easy20_ValidParentheses.isValid("()[]{}"));
        Assert.assertEquals(false, Easy20_ValidParentheses.isValid("(]"));
        Assert.assertEquals(false, Easy20_ValidParentheses.isValid("([)]"));
        Assert.assertEquals(false, Easy20_ValidParentheses.isValid("["));
        Assert.assertEquals(false, Easy20_ValidParentheses.isValid("(("));
        Assert.assertEquals(false, Easy20_ValidParentheses.isValid("){"));
        Assert.assertEquals(false, Easy20_ValidParentheses.isValid(")(){}"));
    }
}