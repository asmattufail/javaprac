package com.javaPrac.LeetCode.Explore.Easy;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class Easy21_MergeTwoSortedListTest {

    @Test
    void mergeTwoLists() {
        //list1
        Easy21_MergeTwoSortedList.ListNode node3 = new Easy21_MergeTwoSortedList.ListNode(4,null);
        Easy21_MergeTwoSortedList.ListNode node2 = new Easy21_MergeTwoSortedList.ListNode(2,node3);
        Easy21_MergeTwoSortedList.ListNode node1 = new Easy21_MergeTwoSortedList.ListNode(1,node2);

        //list2
        Easy21_MergeTwoSortedList.ListNode node33 = new Easy21_MergeTwoSortedList.ListNode(4,null);
        Easy21_MergeTwoSortedList.ListNode node22 = new Easy21_MergeTwoSortedList.ListNode(3,node33);
        Easy21_MergeTwoSortedList.ListNode node11 = new Easy21_MergeTwoSortedList.ListNode(1,node22);

        //mergedList
        Easy21_MergeTwoSortedList.ListNode exp6 = new Easy21_MergeTwoSortedList.ListNode(4,null);
        Easy21_MergeTwoSortedList.ListNode exp5 = new Easy21_MergeTwoSortedList.ListNode(4,exp6);
        Easy21_MergeTwoSortedList.ListNode exp4 = new Easy21_MergeTwoSortedList.ListNode(3,exp5);
        Easy21_MergeTwoSortedList.ListNode exp3 = new Easy21_MergeTwoSortedList.ListNode(2,exp4);
        Easy21_MergeTwoSortedList.ListNode exp2 = new Easy21_MergeTwoSortedList.ListNode(1,exp3);
        Easy21_MergeTwoSortedList.ListNode exp1 = new Easy21_MergeTwoSortedList.ListNode(1,exp2);
        Easy21_MergeTwoSortedList obj = new Easy21_MergeTwoSortedList();

        Easy21_MergeTwoSortedList.ListNode actual = obj.mergeTwoLists(node1,node11);
        for(int i =0; i<6; i++){
            Assert.assertEquals(exp1.val, actual.val);
            exp1 = exp1.next;
            actual = actual.next;
        }

        //list1
        node1 = null;

        //list2
        node11 = new Easy21_MergeTwoSortedList.ListNode(0,null);
    }
}