package com.javaPrac.LeetCode.Explore.Easy;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class Easy66_PlusOneTest {

    @Test
    void plusOne() {
        Assert.assertArrayEquals(new int[]{9,8,7,6,5,4,3,2,1,1},Easy66_PlusOne.plusOne(new int[]{9,8,7,6,5,4,3,2,1,0}));
    }
}