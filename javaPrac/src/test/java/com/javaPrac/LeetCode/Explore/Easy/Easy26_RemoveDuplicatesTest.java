package com.javaPrac.LeetCode.Explore.Easy;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class Easy26_RemoveDuplicatesTest {

    @Test
    void removeDuplicates() {
        int input[] = new int[]{0,0,1,1,1,2,2,3,3,4};
        int output[] = new int[]{0,1,2,3,4};
        Assert.assertEquals(5, Easy26_RemoveDuplicates.removeDuplicates(input));
    }
}