package com.javaPrac.LeetCode.Explore.Easy;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class Easy35_SearchInsertPositionTest {

    @Test
    void searchInsert() {
        Assert.assertEquals(2,Easy35_SearchInsertPosition.searchInsert(new int[]{1,3,5,6},5));
        Assert.assertEquals(1,Easy35_SearchInsertPosition.searchInsert(new int[]{1,3,5,6},2));
        Assert.assertEquals(4,Easy35_SearchInsertPosition.searchInsert(new int[]{1,3,5,6},7));
    }
}