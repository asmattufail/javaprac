package com.javaPrac.LeetCode.Explore.Easy;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Easy28_StrStrTest {

    @Test
    void strStr() {
/*        Assert.assertEquals(2, Easy28_StrStr.strStr("hello","ll"));
        Assert.assertEquals(-1, Easy28_StrStr.strStr("aaaaa","bba"));
        Assert.assertEquals(4, Easy28_StrStr.strStr("mississippi","issip"));*/
        Assert.assertEquals(-1, Easy28_StrStr.strStr("mississippi","issipi"));
    }
}