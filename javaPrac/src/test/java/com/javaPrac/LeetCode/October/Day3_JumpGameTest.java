package com.javaPrac.LeetCode.October;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Day3_JumpGameTest {

    @Test
    void canJump() {
        Assert.assertEquals(true,Day3_JumpGame.canJump(new int[]{2,3,1,1,4}));
        Assert.assertEquals(false,Day3_JumpGame.canJump(new int[]{3,2,1,0,4}));
        Assert.assertEquals(true,Day3_JumpGame.canJump(new int[]{2,0}));
    }
}