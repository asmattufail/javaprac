package com.javaPrac.LeetCode.September;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class Day16_SpiralOrderTest {

    @Test
    void spiralOrder() {
        List<Integer> expected = IntStream.range(1,4).boxed().collect(Collectors.toList());
        Assert.assertEquals(expected, Day16_SpiralOrder.spiralOrder(new int[][]{{1,2,3},{4,5,6},{7,8,9}}));
    }
}