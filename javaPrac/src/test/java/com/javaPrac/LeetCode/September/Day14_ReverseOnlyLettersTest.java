package com.javaPrac.LeetCode.September;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class Day14_ReverseOnlyLettersTest {

    @Test
    void reverseOnlyLetters() {
        Assert.assertEquals("dc-ba", Day14_ReverseOnlyLetters.reverseOnlyLetters("ab-cd"));
        Assert.assertEquals("j-Ih-gfE-dCba", Day14_ReverseOnlyLetters.reverseOnlyLetters("a-bC-dEf-ghIj"));
        Assert.assertEquals("Qedo1ct-eeLg=ntse-T!", Day14_ReverseOnlyLetters.reverseOnlyLetters("Test1ng-Leet=code-Q!"));
        Assert.assertEquals("7_28]", Day14_ReverseOnlyLetters.reverseOnlyLetters("7_28]"));
        Assert.assertEquals("?6E40C", Day14_ReverseOnlyLetters.reverseOnlyLetters("?6C40E"));

    }
}