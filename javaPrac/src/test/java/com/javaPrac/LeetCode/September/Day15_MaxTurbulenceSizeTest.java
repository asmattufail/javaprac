package com.javaPrac.LeetCode.September;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class Day15_MaxTurbulenceSizeTest {

    @Test
    void maxTurbulenceSize() {
        Assert.assertEquals(5, Day15_MaxTurbulenceSize.maxTurbulenceSize(new int[]{9,4,2,10,7,8,8,1,9}));
        Assert.assertEquals(2, Day15_MaxTurbulenceSize.maxTurbulenceSize(new int[]{4,8,12,16}));
        Assert.assertEquals(1, Day15_MaxTurbulenceSize.maxTurbulenceSize(new int[]{1,1,1,1,1}));
        Assert.assertEquals(1, Day15_MaxTurbulenceSize.maxTurbulenceSize(new int[]{100}));
        Assert.assertEquals(2, Day15_MaxTurbulenceSize.maxTurbulenceSize(new int[]{4,5}));
    }
}