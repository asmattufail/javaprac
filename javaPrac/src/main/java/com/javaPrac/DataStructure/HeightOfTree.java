package com.javaPrac.DataStructure;

public class HeightOfTree {

    static class NodeHeight{
        int value;
        NodeHeight right;
        NodeHeight left;

        NodeHeight(int value){
            this.value = value;
        }
    }

    public static void main(String args[]){
        NodeHeight node1 = new NodeHeight(1);
        NodeHeight node2 = new NodeHeight(2);
        NodeHeight node3 = new NodeHeight(3);
        NodeHeight node4 = new NodeHeight(4);
        NodeHeight node5 = new NodeHeight(5);
        /*NodeHeight node6 = new NodeHeight(6);
        NodeHeight node7 = new NodeHeight(7);*/
        node1.left = node2;
        node1.right = node3;
        node2.left = node4;
        node2.right = node5;
        /*node3.right = node6;
        node6.left = node7;*/

        int height = findHeightOfTree(node1);
        System.out.print("Height of Tree is "+ height);
    }

    private static int findHeightOfTree(NodeHeight node) {
        int heightLeft = findSubtreeHeight(node.left);
        int heightRight = findSubtreeHeight(node.right);
        int maxHeight = Math.max(heightLeft,heightRight);
        return maxHeight+1;
    }

    private static int findSubtreeHeight(NodeHeight node) {
        int height;
        if (node != null) {
            height = findHeightOfTree(node);
        } else {
            height = -1;
        }
        return height;
    }
}
