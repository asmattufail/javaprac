package com.javaPrac.Multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class StartThreadsAtSameTime {

    public static AtomicInteger counter = new AtomicInteger(0);

    public static void main(String args[]){
        ExecutorService service = Executors.newFixedThreadPool(10);
        for(int i = 1; i<=10;i++) {
            service.execute(new Task(counter, "Thread" + i));
        }

        service.shutdown();
    }

    static class Task extends Thread{
        AtomicInteger counter;
        Task(AtomicInteger counter, String name){
            super(name);
            this.counter = counter;
        }

        public void run(){
            counter.getAndIncrement();
            synchronized (counter){
                while(counter.intValue() != 10) {
                    try {
                        counter.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                counter.notifyAll();
            }
            System.out.println(" Thread Start Time: "+System.currentTimeMillis());
            for (int i = 1; i <= 10; i++) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(i + " Thread Name: " + Thread.currentThread().getName());
            }
        }
    }
}
