package com.javaPrac.Multithreading;

import java.util.Stack;
//1,2,3
public class QueueImp {
    static Stack<Integer> s1 = new Stack<Integer>();
    static Stack<Integer> s2 = new Stack<Integer>();

    public void add(Integer data){
        while(!s1.empty()){
            s2.push(s1.pop());
        }
        s1.push(data);
        while(!s2.empty()){
            s1.push(s2.pop());
        }
    }

    public Integer remove(){
        if(!s1.empty()) {
            return s1.pop();
        }else{
            return null;
        }
    }

    public static void main(String args[]){
        QueueImp queue = new QueueImp();
        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(4);

        System.out.println(queue.remove());
        System.out.println(queue.remove());
        System.out.println(queue.remove());
    }
}
