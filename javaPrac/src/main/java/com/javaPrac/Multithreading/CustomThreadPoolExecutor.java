package com.javaPrac.Multithreading;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class CustomThreadPoolExecutor {
    private final BlockingQueue<Runnable> queue;
    private final WorkerThread[] workers;
    private final int poolSize;

    public CustomThreadPoolExecutor(int poolSize) {
        this.poolSize = poolSize;
        queue = new LinkedBlockingQueue<>();
        workers = new WorkerThread[poolSize];

        for (int i = 0; i < poolSize; i++) {
            workers[i] = new WorkerThread();
            workers[i].start();
        }
    }

    public void execute(Runnable task) {
        try {
            queue.put(task);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void shutdown() {
        System.out.println("Shutting down thread pool");
        for (int i = 0; i < poolSize; i++) {
            workers[i] = null;
        }
    }

    class WorkerThread extends Thread {
        public void run() {
            while (true) {
                try {
                    queue.take().run();
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        CustomThreadPoolExecutor threadPoolExecutor = new CustomThreadPoolExecutor(10);
        System.out.println(Runtime.getRuntime().availableProcessors());
        for (int i = 1; i <= 7000; i++) {
            Task task = new Task("Task " + i);
            System.out.println("Created : " + task.getName());
            threadPoolExecutor.execute(task);
        }
        threadPoolExecutor.shutdown();
    }

    static class Task implements Runnable {
        private String name;

        public Task(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void run() {
            System.out.println("Executing : " + name + ", Current Seconds : " + new Date().getSeconds() + ", Current Thread : " + Thread.currentThread().getName());
        }
    }

}
