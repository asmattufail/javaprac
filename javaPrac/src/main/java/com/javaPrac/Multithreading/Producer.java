package com.javaPrac.Multithreading;

import java.util.LinkedList;
import java.util.Queue;

public class Producer extends Thread {
    int limit;
    Queue sharedQueue;

    Producer(int limit, Queue sharedQueue){
        this.limit = limit;
        this.sharedQueue = sharedQueue;
    }

    @Override
    public void run() {
        for (int i = 0; i < 7; i++) {
            System.out.println("Produced: " + i);
            try {
                produce(i);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }

        }
    }

    private void produce(int i) throws InterruptedException {

        //wait if queue is full
        while (sharedQueue.size() == limit) {
            synchronized (sharedQueue) {
                System.out.println("Queue is full " + Thread.currentThread().getName()
                        + " is waiting , size: " + sharedQueue.size());

                sharedQueue.wait();
            }
        }

        //producing element and notify consumers
        synchronized (sharedQueue) {
            sharedQueue.add(i);
            sharedQueue.notifyAll();
        }
    }


    public static void main(String args[]){
        Queue queue = new LinkedList();
        Producer producer = new Producer(5,queue);
        Consumer consumer = new Consumer(5, queue);
        producer.start();
        consumer.start();
    }
}
