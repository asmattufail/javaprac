package com.javaPrac.Search;

import java.util.ArrayList;

public class SearchBinaryTree {

    // Let's test our our function
    public static void main(String[] args) {

        BinarySearchTree bst = new BinarySearchTree();

        // Insert all our values
        bst.insert(15);
        bst.insert(12);
        bst.insert(29);
        bst.insert(10);
        bst.insert(25);
        bst.insert(1);

        // Check for the sum starting at the root
        bst.checkForSum(bst.root, 35);
    }
}

  class Node {

    public int value;
    public Node leftNode;
    public Node rightNode; // Will reference other Nodes

    // The default constructor
    Node(int value) {
        this.value = value;
        this.leftNode = null;
        this.rightNode = null;

    }

    // For brevity I am not writing out the setter and getter
    // methods for value, left, and right ... but assume they are
    // available to use

}
///////
 class BinarySearchTree {

    Node root;

    BinarySearchTree() {
        root = null;
    }

    public void inOrder() {
        orderNodes(this.root);
    }

    public void orderNodes(Node node) {

        if (node == null) {
            return;
        }
        orderNodes(node.leftNode);
        orderNodes(node.rightNode);
    }

    public void insert(int newNodeValue) {

        // Always calls the root of the BST
        root = insertRec(root, newNodeValue);
    }

    /**
     * Our recursive function to insert a node into
     * the BST, from the instructions on the question
     *
     * @param root The root of our BST
     * @param value The value associated with the node
     */
    public Node insertRec(Node root, int value) {

        // If the tree is empty, return a new node
        // Setting the root is key to understanding BST

        if (root == null) {
            root = new Node(value);
            return root;

        }

        if (value < root.value) {
            root.leftNode = insertRec(root.leftNode, value);

        } else if (value > root.value) {
            root.rightNode = insertRec(root.rightNode, value);

        }
        return root;
    }

    /**
     * Add all values in the BST into an array to
     * search later
     *
     * @param node The node you want to evaluate
     * @param list The values of the BST in a list
     */
    public ArrayList<Integer> treeToList(Node node, ArrayList<Integer> list) {

        // Stop moving down the BST if node is null
        if (node == null) {
            return list;
        }

        // Will move down the tree starting with the left side.
        // Notice how the node is calling another Node
        // until it finds a node with a left or right as null
        treeToList(node.leftNode, list);
        list.add(node.value); // Adding values to the list
        treeToList(node.rightNode, list);

        return list; // Return the list of node values
    }

    // Pass in the BST to get all the values in a list
    boolean checkForSum(Node node, int target) {

        ArrayList<Integer> emptyList = new ArrayList<>();
        ArrayList<Integer> listToFill = treeToList(node, emptyList);

        int start = 0;
        int end = listToFill.size() - 1;

        while (start < end) {
            if (listToFill.get(start) + listToFill.get(end) == target) {

                System.out.println("Pair found");
                System.out.println(listToFill.get(start) + " " + listToFill.get(end));
                return true;
            }

            // Because we know our values are bigger on the right
            // and smaller on the left we can narrow down our search
            if (listToFill.get(start) + listToFill.get(end) > target) {
                end--;

            }
            if (listToFill.get(start) + listToFill.get(end) < target) {
                start++;
            }
        }

        // Print to the console if no result
        System.out.println("Not found");
        return false;
    }
}
