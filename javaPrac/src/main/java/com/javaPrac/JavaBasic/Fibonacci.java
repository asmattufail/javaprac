package com.javaPrac.JavaBasic;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Fibonacci {
    private static Map<Integer,Integer> memoizeTable = new HashMap<>();
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a number");
        int fibonacciNum = scanner.nextInt();
        int result = fibonacci(fibonacciNum);
        int resultWithMemoize = fibonacciWithMemoize(fibonacciNum);
        System.out.println(result+ " result");
        System.out.println(resultWithMemoize +" resultWithMemoize");
    }

    private static int fibonacci(int num) {
        if( num == 0 ) return 0;
        if( num == 1 ) return 1;
        return fibonacci(num - 1)
                +
                fibonacci(num - 2);
    }

    public static int fibonacciWithMemoize(int num){
        if( num == 0 ) return 0;
        if( num == 1 ) return 1;
        if(memoizeTable.containsKey(num)){
            return memoizeTable.get(num);
        }

        int result = fibonacciWithMemoize(num -1) + fibonacciWithMemoize(num -2);
        memoizeTable.put(num,result);

        return result;
    }

}
