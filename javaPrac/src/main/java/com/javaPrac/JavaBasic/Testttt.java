package com.javaPrac.JavaBasic;

public class Testttt {

    public static void main(String args[]){
        int a[] = {-2, -3, 4, -1, -2, 1, 5, -3};
        int n = a.length;
        int max_sum = maxSubArraySum(a, n);
        System.out.println("Maximum contiguous sum is "
                + max_sum);
    }

    private static int maxSubArraySum(int[] a, int n) {
        int max_sum = a[0];
        int curr_max = a[0];
        for(int i=1; i< n; i++){
            curr_max = Math.max(a[i], curr_max+a[i]);
            max_sum = Math.max(curr_max, max_sum);
        }
        return max_sum;
    }
}
