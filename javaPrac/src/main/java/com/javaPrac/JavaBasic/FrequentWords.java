package com.javaPrac.JavaBasic;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class FrequentWords {
    public static void main(String args[]){
        System.out.println(mostFrequentWords("/home/te/Documents/test.txt", 3));
    }

    public static List<String> mostFrequentWords(String fileName, int n) {
        List<String> topWords = new ArrayList<>();

        try {
            Map<String, Integer> wordMap = readFile(fileName);
            PriorityQueue<Map.Entry<String, Integer>> queue = new PriorityQueue<>(Comparator.comparing(e -> e.getValue()));
            for(Map.Entry<String,Integer> entry: wordMap.entrySet()){
                queue.offer(entry);
                if(queue.size() > n){
                    queue.poll();
                }
            }
            topWords = queue.stream().map(e -> e.getKey()).sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return topWords;
    }

    public static HashMap<String, Integer> readFile(String fileName)
            throws IOException {
        HashMap<String, Integer> wordMap = new HashMap<>();
        File file = new File(fileName);
        FileInputStream fileStream = new FileInputStream(file);
        Scanner sc = new Scanner(fileStream);
        while(sc.hasNextLine()){
            String s = sc.nextLine();
            if(wordMap.containsKey(s)){
                wordMap.put(s, wordMap.get(s)+1);
            }else{
                wordMap.put(s, 1);
            }
        }
        return wordMap;
    }

}
