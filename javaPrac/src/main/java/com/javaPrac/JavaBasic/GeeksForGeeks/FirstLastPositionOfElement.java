package com.javaPrac.JavaBasic.GeeksForGeeks;

public class FirstLastPositionOfElement {

    public static void main(String args[]) {
        int nums[] = new int[]{1, 2, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 6, 6, 6, 7, 7, 7, 7, 8};
        int first = firstIndex(nums, 0, nums.length - 1, 12) +1;
        int last = lastIndex(nums, 0, nums.length - 1, 12) +1 ;
        System.out.println(first+ " first");
        System.out.println(last+" last");

    }

    private static int lastIndex(int[] nums, int left, int right, int target) {
        if(left <= right){
            int mid = left + (right-left)/2;
            if(target == nums[mid] && (mid == nums.length-1 || target < nums[mid+1])){
                return mid;
            }else if(target < nums[mid]){
                return lastIndex(nums, left, mid-1, target);
            }else{
                return lastIndex(nums, mid+1, right, target);
            }
        }
        return -2;
    }

    private static int firstIndex(int[] nums, int left, int right, int target) {
        if(left <= right){
            int mid = left + (right-left)/2;
            if(target == nums[mid] && (mid == 0 || target > nums[mid-1])){
                return mid;
            }else if(target < nums[mid]){
                return firstIndex(nums, left, mid-1, target);
            }else{
                return firstIndex(nums, mid+1, right, target);
            }
        }
        return -2;
    }

}
