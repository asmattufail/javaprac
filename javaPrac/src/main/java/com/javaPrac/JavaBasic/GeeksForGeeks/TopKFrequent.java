package com.javaPrac.JavaBasic.GeeksForGeeks;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.counting;

public class TopKFrequent {

    public static void main(String args[]){
        int[] nums = new int[]{2,4,1,5,4,3,33,77,22,33,12,33,22,11,44,66,34,33};
        List<Integer> res = topKFrequent(nums, 3);
        System.out.println(res);

    }

    /*private static List<Integer> topKFrequent(int[] nums, int k) {
        Map<Integer,Long> freq = Arrays.stream(nums).boxed().collect(groupingBy(Function.identity(),counting()));

        // create a min heap
        PriorityQueue<Map.Entry<Integer,Long>> queue = new PriorityQueue<>(Comparator.comparing(e->e.getValue()));

        //maintain a heap of size k.
        for (Map.Entry<Integer, Long> entry : freq.entrySet()) {
            queue.offer(entry);
            if(queue.size() > k){
                queue.poll();
            }
        }
        //get all elements from the heap
        List<Integer> result = new ArrayList<>();
        while (queue.size() > 0) {
            result.add(queue.poll().getKey());
        }

        //reverse the order
        Collections.reverse(result);
        return result;
    }*/

    private static List<Integer> topKFrequent(int[] nums, int k) {
        Map<Integer,Long> freq = Arrays.stream(nums).boxed().collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
        PriorityQueue<Map.Entry<Integer,Long>> queue = new PriorityQueue<>(Comparator.comparing(e -> e.getValue()));

        for(Map.Entry<Integer,Long> entry: freq.entrySet()){
            queue.offer(entry);
            if(queue.size() > k){
                queue.poll();
            }
        }

        List<Integer> result = queue.stream().map(e-> e.getKey()).sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        return result;
    }
}
