package com.javaPrac.JavaBasic.GeeksForGeeks;

import java.util.*;
import java.util.LinkedList;

public class LRUCache {

    private Deque<String> queue = null;
    private Set<String> set = new HashSet<>();
    private int limit;

    LRUCache(int limit){
        this.limit = limit;
        queue = new LinkedList<>();
    }

    public void get(String data){
        if(set.contains(data)){
            Iterator itr = queue.iterator();
            while(itr.hasNext()){
                String s = (String)itr.next();
                if(s.equals(data)){
                    queue.remove(s);
                    break;
                }
            }
            queue.addFirst(data);
        }else{
            if(queue.size() == limit){
                queue.pollLast();
            }
            queue.addFirst(data);
        }
        set.add(data);
    }

    public void display()
    {
        Iterator itr = queue.iterator();
        while (itr.hasNext()) {
            System.out.print(itr.next() + " ");
        }
    }

    public static void main(String args[]){
        LRUCache ca = new LRUCache(4);
        ca.get("1");
        ca.get("2");
        ca.get("3");
        ca.get("1");
        ca.get("4");
        ca.get("5");
        ca.display();
    }
}
