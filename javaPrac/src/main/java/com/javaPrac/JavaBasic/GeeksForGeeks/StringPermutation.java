package com.javaPrac.JavaBasic.GeeksForGeeks;

public class StringPermutation {

    public static void main(String args[]){
        String str = "ABC";
        permute(str,"");
    }

    public static void permute(String str, String ans){
        if(str.length() == 0){
            System.out.println(ans);
        }

        for(int i =0; i< str.length(); i++){
            char c = str.charAt(i);

            String temp = str.substring(0,i)+str.substring(i+1);
            permute(temp,ans+c);
        }
    }
}
