package com.javaPrac.JavaBasic.GeeksForGeeks.LinkedList;

public class LinkedList {
    private static Node first;
    private static Node last;
    public static class Node{
        private int data;
        private Node prev;
        private Node next;

        public Node(int data){
            this.data = data;
            this.prev = null;
            this.next = null;
        }

        public int getData() {
            return this.data;
        }
    }

    public static Node getFirst() {
        return first;
    }

    public static Node getLast() {
        return last;
    }

    public static void main(String args[]){
        LinkedList list = new LinkedList();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);

        printList(list.reverseRecursively(first, null));
    }

    public static void add(int data){
        Node node = new Node(data);
        if(first == null){
            first = node;
            last = node;
        }else {
            Node l = last;
            last = node;
            l.next = node;
            last.prev = l;
        }
    }

    public static int size(Node root){
        Node ptr = root;
        int size = 0;
        while(ptr != null){
            size = size+1;
            ptr = ptr.next;
        }
        return size;
    }

    /* Given a key, deletes the first occurrence of key in linked list */
    public static void deleteNode(int data){
        Node ptr = getFirst();
        while(ptr != null){
            if(ptr.data == data){
                if(ptr == first){
                    first = ptr.next;
                }
                if(ptr == last){
                    last = ptr.prev;
                }
            }else{
                ptr = ptr.next;
            }
        }
    }

    public Node reverse(Node root){
        last = first;
        Node curr = root;
        Node prev = null;
        Node next;
        while(curr != null){
            next = curr.next;
            curr.next = prev;
            prev = curr;
            curr = next;
        }
        root = prev;
        first= root;
        return root;
    }

    public Node reverseRecursively(Node node, Node prev){
        if(node == null){
            first = prev;
            return first;
        }
        last = first;
        Node next = node.next;
        node.next = prev;
        return reverseRecursively(next, node);
    }

    static void printList(Node node)
    {
        while (node != null) {
            System.out.print(node.data + " ");
            node = node.next;
        }
        System.out.println();
        System.out.println(first.data + " first");
        System.out.println(last.data + " last");
    }

}
