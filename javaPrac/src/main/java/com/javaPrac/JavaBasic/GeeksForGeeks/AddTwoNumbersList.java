package com.javaPrac.JavaBasic.GeeksForGeeks;

import java.util.List;

/*Input:
        List1: 5->6->3 // represents number 365
        List2: 8->4->2 // represents number 248
        Output:
        Resultant list: 3->1->6 // represents number 613
        Explanation: 365 + 248 = 613*/
public class AddTwoNumbersList {
    static class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
  }

    public static ListNode addTwoList(ListNode list1, ListNode list2){
        ListNode current1 = list1;
        ListNode current2 = list2;
        ListNode result = null;
        ListNode current = result;
        int remainder = 0;
        while(current1 != null || current2 != null){
            int sum = 0;
            if(current1 != null) {
                sum = current1.val;
                current1 = current1.next;
            }
            if(current2 != null){
                sum = sum + current2.val;
                current2 = current2.next;
            }
            sum = sum + remainder;
            int resultNode = sum % 10;
            remainder = sum / 10;
            ListNode node = new ListNode(resultNode, null);
            if(result == null){
                result = node;
                current = result;
            }else {
                current.next = node;
                current = current.next;
            }

        }
        return result;

    }

    public static void printList(ListNode node){
        while(node != null){
            System.out.print(node.val + " ");
            node = node.next;
        }
    }

    public static void main(String args[]){
        ListNode node1 = new ListNode(5);
        ListNode node2 = new ListNode(6);
        ListNode node3 = new ListNode(3);
        node1.next = node2;
        node2.next = node3;

        ListNode node4 = new ListNode(8);
        ListNode node5 = new ListNode(4);
        ListNode node6 = new ListNode(2);
        node4.next = node5;
        node5.next = node6;
        ListNode result = addTwoList(node1, node4);
        printList(result);
    }
}
