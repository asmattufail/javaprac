package com.javaPrac.JavaBasic.GeeksForGeeks;

public class NthNodeFromEnd {
    private static Node root;
    static class Node{
        int data;
        Node next;

        Node(int data){
            this.data = data;
        }
    }

    public static Node nthNodeFromEnd(int n){
        Node ptr1 = NthNodeFromEnd.root;
        Node ptr2 = NthNodeFromEnd.root;
        for(int i =1; i< n; i++){
            ptr1 = ptr1.next;
        }
        while(ptr1.next != null){
            ptr1= ptr1.next;
            ptr2 = ptr2.next;
        }
        return ptr2;
    }

    public static void main(String args[]){
        Node root = new Node(1);
        root.next = new Node(2);
        root.next.next = new Node(3);
        root.next.next.next = new Node(4);
        root.next.next.next.next = new Node(5);
        NthNodeFromEnd.root = root;
        System.out.print(nthNodeFromEnd(4).data);
    }
}
