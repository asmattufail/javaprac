package com.javaPrac.JavaBasic.GeeksForGeeks;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class BinaryTree {
    private Node root;
    static class Node{
        private Integer data;
        private Node left, right;

        Node(int data){
            this.data = data;
            right = left = null;
        }
    }

    public void add(Integer data){
        root = addRecursive(root, data);
    }

    private Node addRecursive(Node node, Integer data) {
        if(node == null){
            return new Node(data);
        }
        if(data < node.data){
            node.left = addRecursive(node.left, data);
        }else if(data > node.data){
            node.right = addRecursive(node.right, data);
        }else{
            return node;
        }
        return node;
    }

    private Node addWithoutRecursion(Integer data){
        Node node = new Node(data);
        if(root == null){
            root = node;
            return root;
        }
        Queue<Node> queue = new LinkedList<>();
        queue.add(root);

        while(!queue.isEmpty()){
            Node temp = queue.remove();
            if(temp.data < data){
                if(temp.left == null){
                    temp.left = node;
                    break;
                }else{
                    queue.add(temp.left);
                }
            }else{
                if(temp.right == null){
                    temp.right = node;
                    break;
                }else{
                    queue.add(temp.right);
                }
            }
        }
        return root;
    }

    public void preOrderRecursive(Node node){
        if(node == null)
            return;
        System.out.print(node.data+" ");
        preOrderRecursive(node.left);
        preOrderRecursive(node.right);
    }

    public void preOrderTraversal(Node node){
        if(node == null)
            return;
        Node current = node;
        Stack<Node> stack = new Stack<>();
        while(!stack.isEmpty() || current != null){
            if(current != null) {
                System.out.print(current.data+" ");
                stack.push(current);
                current = current.left;
            }else{
                current =  stack.pop();
                current = current.right;
            }
        }

    }

    public void inOrderRecursive(Node node){
        if (node == null)
            return;
       inOrderRecursive(node.left);
       System.out.print(node.data+" ");
       inOrderRecursive(node.right);
    }

    public void inOrderTraversal(Node root){
        if (root == null)
            return;
        Stack<Node> stack = new Stack<>();
        Node current = root;
        while(!stack.isEmpty() || current != null){
            if(current != null){
                stack.push(current);
                current = current.left;
            }else{
                current = stack.pop();
                System.out.print(current.data+" ");
                current = current.right;
            }
        }
    }

    public void postOrderRecursive(Node root){
        if(root == null)
            return;
        postOrderRecursive(root.left);
        postOrderRecursive(root.right);
        System.out.print(root.data+" ");
    }

    public void postOrderTraversal(Node root){
        if(root == null)
            return;

        Stack<Node> stack = new Stack<>();
        Stack<Node> out = new Stack<>();
        stack.push(root);
        while(!stack.isEmpty()){
            Node current = stack.pop();
            out.push(current);
            if(current.left != null){
                stack.push(current.left);
            }
            if(current.right != null){
                stack.push(current.right);
            }
        }
        while(!out.isEmpty()){
            System.out.print(out.pop().data+" ");
        }
    }


    public static void main(String args[]){
        /*4-2
                    4
               _____|_____
              |          |
              2          6
         _____|____  ____|_____
        |         | |         |
        1         3 5         7

        1234567 */
        BinaryTree tree = new BinaryTree();
        tree.add(4);
        tree.add(2);
        tree.add(1);
        tree.add(3);
        tree.add(6);
        tree.add(5);
        tree.add(7);
        System.out.print("PreOrder Recursive: ");
        tree.preOrderRecursive(tree.root);
        System.out.println();
        System.out.print("PreOrder Iterative: ");
        tree.preOrderTraversal(tree.root);
        System.out.println();
        System.out.print("Inorder Iterative: ");
        tree.inOrderTraversal(tree.root);
        System.out.println();
        System.out.print("Inorder Recursive: ");
        tree.inOrderRecursive(tree.root);
        System.out.println();
        System.out.print("PostOrder Iterative: ");
        tree.postOrderTraversal(tree.root);
        System.out.println();
        System.out.print("PostOrder Recursive: ");
        tree.postOrderRecursive(tree.root);
        System.out.println();
    }
}
