package com.javaPrac.JavaBasic.GeeksForGeeks.LinkedList;

import java.util.HashSet;

public class RemoveLoopInLinkedList {
    private Node root;
    static class Node{
        private Integer data;
        private Node next;

        Node(int data){
            this.data = data;
        }
    }

    public Node add(Node node){
        if(root == null){
            root = node;
            return root;
        }
        Node current = root;
        while(current.next != null){
            current = current.next;
        }
        current.next = node;
        return node;
    }

    public void removeLoop(Node root){
        if(root != null){
            HashSet<Integer> set = new HashSet();
            Node current = root;
            while(current.next != null){
                if(!set.contains(current.next.data)) {
                    set.add(current.next.data);
                }else{
                    current.next = null;
                    break;
                }
                current = current.next;
            }
        }
    }

    public void printList(Node root){
        Node current = root;
        while(current != null){
            System.out.print(current.data+" ");
            current = current.next;
        }
    }

    public static void main(String args[]){
        RemoveLoopInLinkedList list = new RemoveLoopInLinkedList();
        Node node1 = list.add(new Node(2));
        Node node2 = list.add(new Node(5));
        Node node3 = list.add(new Node(9));
        Node node4 = list.add(new Node(10));
        Node node5 = list.add(node2);
        list.removeLoop(list.root);
        System.out.println("After loop detection");
        list.printList(list.root);
    }
}
