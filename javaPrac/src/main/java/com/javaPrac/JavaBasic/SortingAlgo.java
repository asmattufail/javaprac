package com.javaPrac.JavaBasic;

import java.util.Arrays;

public class SortingAlgo {

    public static void main(String[] args) {
        int num[] = {4, 2, 1, 5, 3};
        //bubbleSort(num);
        //selectionSort(num);
        //insertionSort(num);
        //mergeSort(num);
        quickSort(num);
    }

    private static void bubbleSort(int[] num) {
        boolean sorted = false;
        while(!sorted){
            sorted = true;
            for(int i = 0; i< num.length -1; i++){
                if( num[i] > num[i+1]){
                    sorted = false;
                    int temp = num[i];
                    num[i] = num[i+1];
                    num[i+1] = temp;
                }
            }
        }
        Arrays.stream(num).forEach(i -> System.out.print(i+" "));
    }

    private static void selectionSort(int[] num) {
        for(int i = 0; i< num.length; i++){
            int minIndex = i;
            int min = num[i];
            for(int j = i+1; j< num.length; j++){
                if(num[j] < min){
                    min = num[j];
                    minIndex = j;
                }
            }
            int temp = num[i];
            num[i] = min;
            num[minIndex] = temp;
        }
        Arrays.stream(num).forEach(i -> System.out.print(i+" "));
    }

    private static void insertionSort(int[] num) {
        for(int i = 1; i< num.length; i++){
            int current = num[i];
            int j =i-1;
            while(j>=0 && current < num[j]){
                num[j+1] = num[j];
                j--;
            }
            num[j+1] = current;
        }
        Arrays.stream(num).forEach(i -> System.out.print(i+" "));
    }

    private static void mergeSort(int[] num) {
        mergeSort(num, new int[num.length], 0, num.length -1);
        Arrays.stream(num).forEach(i -> System.out.print(i+" "));
    }

    private static void mergeSort(int[] num, int[] temp, int leftStart, int rightEnd) {
        if(leftStart >= rightEnd){
            return;
        }
        int middle = (leftStart+rightEnd)/2;
        mergeSort(num, temp, leftStart, middle);
        mergeSort(num, temp, middle+1, rightEnd);
        merge(num, temp, leftStart, rightEnd);
    }

    private static void merge(int[] array, int[] temp, int leftStart, int rightEnd) {
        int leftEnd = (rightEnd + leftStart)/2;
        int rightStart = leftEnd + 1;
        int size = rightEnd - leftStart +1;

        int left = leftStart;
        int right = rightStart;
        int index = leftStart;

        while(left <= leftEnd && right <= rightEnd){
            if(array[left] <= array[right]){
                temp[index] = array[left];
                left++;
            }else{
                temp[index] = array[right];
                right++;
            }
            index++;
        }

        System.arraycopy(array, left, temp, index, leftEnd-left+1);
        System.arraycopy(array, right, temp, index, rightEnd-right+1);
        System.arraycopy(temp, leftStart, array, leftStart, size);
    }

    private static void quickSort(int[] num) {
        quickSort(num, 0, num.length-1);
    }

    private static void quickSort(int[] array, int begin, int end) {
        if(begin >= end){
            return;
        }
        int pivot = partition(array, begin, end);
        quickSort(array, begin, pivot-1);
        quickSort(array, pivot+1, end);
    }

    private static int partition(int[] array, int begin, int end) {
        int pivot = end;
        return 0;
    }


}
