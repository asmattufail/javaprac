package com.javaPrac.LeetCode.October;

public class Day3_JumpGame {
    /*Input: nums = [2,3,1,1,4]
    Output: true
    Input: nums = [3,2,1,0,4]
    Output: false*/
    public static boolean canJump(int[] nums) {
        int maxReach = 0;
        int i =0;
        while(i < nums.length-1 && i<=maxReach){
            maxReach = Integer.max(i + nums[i], maxReach);
            i++;
        }
        if(maxReach >= nums.length-1)
            return true;
        return false;
    }
}
