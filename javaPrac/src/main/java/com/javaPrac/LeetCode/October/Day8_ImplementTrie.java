package com.javaPrac.LeetCode.October;

import java.util.ArrayList;
import java.util.List;

public class Day8_ImplementTrie {
    class Trie {
        List<String> trie = null;
        public Trie() {
            trie = new ArrayList();
        }

        public void insert(String word) {
            if(!trie.contains(word))
                trie.add(word);
        }

        public boolean search(String word) {
            if(trie.contains(word))
                return true;
            return false;
        }

        public boolean startsWith(String prefix) {
            return trie.stream().anyMatch(x-> x.startsWith(prefix));
        }
    }
}
