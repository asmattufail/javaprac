package com.javaPrac.LeetCode.September;

import java.util.ArrayList;
import java.util.List;

public class Day16_SpiralOrder {
    /*Input: matrix = [[1,2,3],[4,5,6],[7,8,9]]
        Output: [1,2,3,6,9,8,7,4,5]*/

    public static List<Integer> spiralOrder(int[][] matrix) {
        List<Integer> result = new ArrayList<>();
        int row = matrix.length;
        int col = matrix[0].length;

        int rowPointerLeft = 0;
        int rowPointerRight = row;
        int columnPointerLeft = 0;
        int columnPointerRight = col-1;
        List<String> indexList = new ArrayList<>();
        for(int i = 0; i<matrix.length; i++){
            for(int j = 0; j<matrix[i].length; j++){
                indexList.add(i+""+j);
            }
        }
        while(!indexList.isEmpty()){
            while(columnPointerLeft < matrix[rowPointerLeft].length) {
                result.add(matrix[rowPointerLeft][columnPointerLeft]);
                indexList.remove(rowPointerLeft + "" + columnPointerLeft);
                columnPointerLeft++;
            }
          result.forEach(System.out::print);
        }
        return result;
    }
}
