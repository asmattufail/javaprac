package com.javaPrac.LeetCode.September;

public class Day14_ReverseOnlyLetters {
    /*Example 1:
    Input: s = "ab-cd" -> "db-cd" -> "db-ca" -> "dc-ca" -> "dc-ba"
    Output: "dc-ba"

    Input: s = "a-bC-dEf-ghIj"
    Output: "j-Ih-gfE-dCba"

    Input: s = "Test1ng-Leet=code-Q!"
    Output: "Qedo1ct-eeLg=ntse-T!"*/

    public static String reverseOnlyLetters(String s) {
        if(s == null)
            return null;
        char[] ch = s.toCharArray();
        int leftPointer = 0;
        int rightPointer = s.length()-1;
        while(leftPointer < rightPointer){
            if(Character.isLetter(ch[leftPointer]) && Character.isLetter(ch[rightPointer])) {
                char temp = ch[leftPointer];
                ch[leftPointer] = ch[rightPointer];
                ch[rightPointer] = temp;
                leftPointer++;
                rightPointer--;
            }else if(!Character.isLetter(ch[leftPointer])){
                leftPointer++;
            }else if(!Character.isLetter(ch[rightPointer])){
                rightPointer--;
            }
        }
        return String.valueOf(ch);
    }
}
