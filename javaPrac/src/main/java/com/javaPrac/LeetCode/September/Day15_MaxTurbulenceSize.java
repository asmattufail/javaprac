package com.javaPrac.LeetCode.September;

public class Day15_MaxTurbulenceSize {
    /*Input: arr = [9,4,2,10,7,8,8,1,9]
    Output: 5
    Explanation: arr[1] > arr[2] < arr[3] > arr[4] < arr[5]*/

    public static int maxTurbulenceSize(int[] arr) {
        if(arr.length <= 1){
            return arr.length;
        }
        int maxTurbSize = 1;
        int j=1;
        int currentTurbulence = 1;
        for(int i=j; i<arr.length; i++){
            if((i+1)<arr.length && ((arr[i-1] > arr[i] && arr[i] < arr[i+1]) || (arr[i-1] < arr[i] && arr[i] > arr[i+1]))){
                if(currentTurbulence ==1){
                    currentTurbulence =currentTurbulence+2;
                }else {
                    currentTurbulence++;
                }
                j++;
                continue;
            }else if(i<arr.length && currentTurbulence ==1 && (arr[i-1] > arr[i] || arr[i-1] < arr[i])){
                currentTurbulence++;
            }else if((i+1)<arr.length && currentTurbulence ==1 && (arr[i+1] > arr[i] || arr[i+1] < arr[i])){
                currentTurbulence++;
            }
            maxTurbSize = Math.max(currentTurbulence,maxTurbSize);
            currentTurbulence = 1;
            j++;
        }
        return maxTurbSize;
    }
}
