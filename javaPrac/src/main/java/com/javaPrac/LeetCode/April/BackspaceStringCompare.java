package com.javaPrac.LeetCode.April;

public class BackspaceStringCompare {
    public static void main(String args[]){
        backspaceCompare("y#fo##f","y#f#o##f");
    }
    public static void backspaceCompare(String S, String T) {
        StringBuilder s = new StringBuilder(S);
        StringBuilder t = new StringBuilder(T);

        if (deleteBackSpaceChar(s).compareTo(deleteBackSpaceChar(t))==0) {
            System.out.println(true);
        }else{
            System.out.println(false);
        }
    }

    private static String deleteBackSpaceChar(StringBuilder s) {
        int i=0;
        while (s.length() > 0 && i < s.length()) {
            if (i > 0 && s.charAt(i) == '#' && s.charAt(i - 1) != '#') {
                s.deleteCharAt(i);
                s.deleteCharAt(i - 1);
                i = i - 2;
            } else if (i == 0 && s.charAt(i) == '#') {
                s.deleteCharAt(i);
                i = i - 1;
            }
            i++;
        }
        return s.toString();
    }
}
