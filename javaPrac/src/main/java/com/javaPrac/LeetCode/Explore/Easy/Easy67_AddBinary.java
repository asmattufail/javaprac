package com.javaPrac.LeetCode.Explore.Easy;

public class Easy67_AddBinary {
    /*Input: a = "11", b = "1"
        Output: "100"*/
    public static String addBinary(String a, String b) {
        int b1 = a.length()-1;
        int b2 = b.length()-1;

//        if(b1 == 0 && b2 == 0){
//            return "0";
//        }
        int i = 0, carry = 0, sum =0;
        StringBuilder sb = new StringBuilder();
        while (b1 >= 0 || b2 >= 0)
        {
            int i1 = b1 >= 0 ?(a.charAt(b1) - '0') % 10 :0  ;
            int i2 = b2 >= 0 ? (b.charAt(b2) - '0') % 10: 0;
            sum = (i1 + i2 + carry) % 2;

            carry = (i1 + i2 + carry) / 2;
            sb.append(sum);
            b1--;
            b2--;
        }
        if (carry != 0) {
            sb.append(carry);
        }
        return sb.reverse().toString();
    }
}
