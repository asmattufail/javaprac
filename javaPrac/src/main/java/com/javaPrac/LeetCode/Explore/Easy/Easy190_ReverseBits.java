package com.javaPrac.LeetCode.Explore.Easy;

public class Easy190_ReverseBits {
    public static void main(String args[]){
        reverseBits(13);
    }

    private static void reverseBits(int num){
        int res = 0;
        for(int i =0; i<31; i++){
            System.out.println(Integer.toBinaryString(res));
            res = res << 1 ;
            System.out.println(Integer.toBinaryString(res));
            res = res | (num&1);
            System.out.println(Integer.toBinaryString(res));
            num = num >> 1;
            System.out.println(Integer.toBinaryString(num));
        }
        System.out.print(res);
    }
}
