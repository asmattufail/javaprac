package com.javaPrac.LeetCode.Explore.Easy;

import java.util.HashMap;
import java.util.Map;

public class Easy27_RemoveElement {
    /*Input: nums = [3,2,2,3], val = 3
    Output: 2, nums = [2,2,_,_]*/
    public static int removeElement(int[] nums, int val) {
        int count = 0;
        for(int i =0; i< nums.length; i++){
            int current = nums[i];
            if(current != val){
                nums[count] = current;
                count++;
            }
        }
        return count;
    }

    public static class Easy13_RomanToInt {
        private static Map<Character,Integer> romanIntMap = new HashMap<>();
        static{
            createRomanIntMap();
        }

        /*Input: s = "MCMXCIV"
          Output: 1994
          Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.*/
        public static int romanToInt(String s) {
            Integer result = 0;
            for(int i=0; i<s.length(); i++){
                int current = romanIntMap.get(s.charAt(i));
                if(i+1 <s.length()) {
                    int next = romanIntMap.get(s.charAt(i + 1));
                    if (current < next) {
                        result = result - current + next;
                        i++;
                    } else {
                        result = result + current;
                    }
                }else{
                    result = result + current;
                }
            }
            return result;
        }

        private static void createRomanIntMap() {
            romanIntMap.put('I',1);
            romanIntMap.put('V',5);
            romanIntMap.put('X',10);
            romanIntMap.put('L',50);
            romanIntMap.put('C',100);
            romanIntMap.put('D',500);
            romanIntMap.put('M',1000);
        }


    }
}
