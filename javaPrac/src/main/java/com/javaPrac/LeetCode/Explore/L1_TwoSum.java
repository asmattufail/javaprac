package com.javaPrac.LeetCode.Explore;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class L1_TwoSum {

    public static void main(String args[]){
        int nums[] = new int[]{3,2,4};
        int target =6;
        int[] result = (twoSum(nums,target));
        Arrays.stream(result).forEach(System.out::println);

    }

    public static int[] twoSum(int[] nums, int target) {
        Map<Integer,Integer> indexMap = new HashMap<>();
        for(int i= 0; i< nums.length; i++){
            indexMap.put(nums[i],i);
        }
        boolean found = false;
        int j=0;
        Integer resultIndex = null;
        while(!found){
            resultIndex = indexMap.get(target-nums[j]);
            if(resultIndex != null && resultIndex!= j){
                found = true;
                break;
            }
            j++;
        }
        if(found){
            return new int[]{j,resultIndex};
        }else{
            return null;
        }

    }
}
