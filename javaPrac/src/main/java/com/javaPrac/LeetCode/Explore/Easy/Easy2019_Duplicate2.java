package com.javaPrac.LeetCode.Explore.Easy;

import java.util.HashMap;
import java.util.Map;

public class Easy2019_Duplicate2 {
    public boolean containsNearbyDuplicate(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        for(int i=0; i<nums.length; i++){
            int current = nums[i];
            if(map.containsKey(current) && (i-map.get(current)<=k)){
                return true;
            }else{
                map.put(nums[i],i);
            }
        }
        return false;
    }
}
