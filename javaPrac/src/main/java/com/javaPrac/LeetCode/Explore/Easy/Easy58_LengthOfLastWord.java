package com.javaPrac.LeetCode.Explore.Easy;

public class Easy58_LengthOfLastWord {
    /*Input: s = "   fly me   to   the moon  "
    Output: 4*/
    public static int lengthOfLastWord(String s) {
        if(s == null || s.length() == 0)
            return 0;
        int i =s.length()-1;
        while(i>=0 && s.charAt(i) == ' '){
            i--;
        }
        int j =0;
        while(i>=0 && s.charAt(i) != ' '){
            j++;
            i--;
        }
        return j;
    }
}
