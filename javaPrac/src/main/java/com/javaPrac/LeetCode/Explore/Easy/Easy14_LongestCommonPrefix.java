package com.javaPrac.LeetCode.Explore.Easy;

public class Easy14_LongestCommonPrefix {

    public static String longestCommonPrefix(String[] strs) {
        String result = strs[0];
        for(int i = 0; i< strs.length-1; i++){
            result = getCommonPrefix(result, strs[i+1]);
        }
        return result;
    }
//geeksforgeeks, geeksklk
    private static String getCommonPrefix(String current, String next) {
        int l1 = current.length();
        int l2 = next.length();
        int l = l1 < l2 ? l1 : l2;
        StringBuilder commonPrefix = new StringBuilder("");
        for(int i=0; i<l; i++){
            char c = current.charAt(i);
            if(current.charAt(i) == next.charAt(i)){
                commonPrefix = commonPrefix.append(c);
            }else{
                break;
            }
        }
        return commonPrefix.toString();
    }
}
