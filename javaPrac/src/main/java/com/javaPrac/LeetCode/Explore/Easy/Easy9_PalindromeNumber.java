package com.javaPrac.LeetCode.Explore.Easy;

public class Easy9_PalindromeNumber {
    public static boolean isPalindrome(int x) {
        if(x < 0)
            return false;
        long reversed = 0;
        int y = x;
        while(y != 0){
            reversed = reversed * 10 + y % 10;
            y = y/10;
        }
        return x == reversed;
    }
    public static boolean isPalindrome1(int x) {
        if(x < 0)
            return false;
        String palind = String.valueOf(x);
        int pointer1 = 0;
        int pointer2 = palind.length()-1;
        while(pointer1 <= pointer2){
            if(palind.charAt(pointer1) == palind.charAt(pointer2)){
                pointer1++;
                pointer2--;
            }else{
                return false;
            }
        }
        return true;
    }
}
