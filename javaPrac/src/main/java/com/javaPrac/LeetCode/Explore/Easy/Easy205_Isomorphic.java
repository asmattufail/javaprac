package com.javaPrac.LeetCode.Explore.Easy;

public class Easy205_Isomorphic {

    public static void main(String args[]){
        System.out.print(isIsomorphic("bbbaaaba","aaabbbba"));
    }

    public static boolean isIsomorphic(String s, String t) {
        char[] charS = s.toCharArray();
        char[] charT = t.toCharArray();
        int[] preIndexCharS = new int[256];
        int[] preIndexCharT = new int[256];

        for(int i = 0; i < charS.length; i++) {
            if(preIndexCharS[charS[i]] != preIndexCharT[charT[i]]) {
                return false;
            }
            preIndexCharS[charS[i]] = i + 1;
            preIndexCharT[charT[i]] = i + 1;
        }

        return true;
    }
}
