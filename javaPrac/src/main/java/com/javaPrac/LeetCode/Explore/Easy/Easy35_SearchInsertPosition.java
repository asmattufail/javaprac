package com.javaPrac.LeetCode.Explore.Easy;

public class Easy35_SearchInsertPosition {
    /*Input: nums = [1,3,5,6], target = 5
    Output: 2*/
    public static int searchInsert(int[] nums, int target) {
        int left =0, right= nums.length-1;
        int index = binarySearch(nums,left,right,target);
        return index;
    }

    private static int binarySearch(int[] nums, int left, int right, int target){
        if(right<=left) {
            if(target > nums[right]){
                return right+1;
            }
            return left;
        }
        int  mid = left + (right-left)/2;
        if(target > nums[mid]){
            return binarySearch(nums,mid+1,right,target);
        }else if(target < nums[mid]){
            return binarySearch(nums,left,mid,target);
        }else if(target == nums[mid]){
            return mid;
        }else{
            return -1;
        }
    }

}
