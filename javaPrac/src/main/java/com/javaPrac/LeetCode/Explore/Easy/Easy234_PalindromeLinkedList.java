package com.javaPrac.LeetCode.Explore.Easy;

import java.util.LinkedList;

public class Easy234_PalindromeLinkedList {
    static class ListNode {
     int val;
     ListNode next;
     ListNode() {}
     ListNode(int val) { this.val = val; }
     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 }
    public static void main(String args[]){
        isPalindrome(new ListNode(1));
    }
    public static boolean isPalindrome(ListNode head) {
        LinkedList<ListNode> list = new LinkedList();
        ListNode current = head;
        while(current != null){
            list.add(current);
            current = current.next;
        }
        list.peekFirst();
        while(list.size() != 0){
            if(list.peekFirst() == list.peekLast()){
                list.pollFirst();
                list.pollLast();
            }else{
                return false;
            }
        }
        return true;
    }
}
