package com.javaPrac.LeetCode.Explore.Easy;

import java.util.*;

public class Easy20_ValidParentheses {
//s = "()"
    public static boolean isValid(String s) {
        char[] ch = s.toCharArray();
        List<Character> in = Arrays.asList('(','[','{');
        List<Character> out = Arrays.asList(')',']','}');
        Map<Character,Character> map = new HashMap<>();
        map.put(')','(');
        map.put(']','[');
        map.put('}','{');
        Stack<Character> symbolStack = new Stack<>();
        for (int i=0; i<ch.length; i++){
            if(in.contains(ch[i])){
                symbolStack.push(ch[i]);
            }else if(symbolStack.empty() || (out.contains(ch[i]) && !symbolStack.pop().equals(map.get(ch[i])))){
                return false;
            }
        }
        if(!symbolStack.empty())
            return false;
        return true;
    }
}
