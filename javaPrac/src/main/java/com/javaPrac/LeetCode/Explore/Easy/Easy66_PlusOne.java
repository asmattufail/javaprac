package com.javaPrac.LeetCode.Explore.Easy;

public class Easy66_PlusOne {
    /*Input: digits = [1,2,3]
    Output: [1,2,4]*/
    public static int[] plusOne(int[] digits) {
        int carry = 1;
        for(int i = digits.length-1; i >=0; i--){
            digits[i] += carry;
            if(digits[i] > 9){
                carry = digits[i] / 10;
                digits[i] = digits[i] % 10;
            }else{
                carry = 0;
                break;
            }
        }
        if(carry == 0){
            return digits;
        }else{
            int[] result = new int[digits.length+1];
            result[0] = carry;
            for(int i=0; i<digits.length; i++){
                result[i+1] = digits[i];
            }
            return result;
        }
    }
}
