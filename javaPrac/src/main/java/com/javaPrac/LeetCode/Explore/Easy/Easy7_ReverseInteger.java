package com.javaPrac.LeetCode.Explore.Easy;

public class Easy7_ReverseInteger {
    /*Input: x = 123
    Output: 321
    Input: x = -123
    Output: -321
    Input: x = 120
    Output: 21*/
    public static int reverse(int x) {
        long reversed = 0;
        while(x != 0){
            int remainder = x % 10;
            reversed = reversed * 10 + remainder;
            x = x/10;
            if(reversed > Integer.MAX_VALUE || reversed < Integer.MIN_VALUE)
                return 0;
        }
        return (int)reversed;
    }
}
