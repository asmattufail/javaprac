package com.javaPrac.LeetCode.Explore.Easy;

public class Easy28_StrStr {
    /*Input: haystack = "hello", needle = "ll"
    Output: 2*/
    public static int strStr(String haystack, String needle) {
        if(needle.trim().length() == 0){
            return 0;
        }
        if(needle.trim().length() > haystack.trim().length()){
            return -1;
        }
        char[] source = haystack.toCharArray();
        char[] target = needle.toCharArray();
        int i = 0, j = 0, index = -1, start =0;
        while(i<haystack.length() && j < needle.length() && start<haystack.length()){
            if(source[start] == target[j]){
                if(j==0){
                    index = start;
                }
                j++;
                start++;
            }else{
                j=0;
                i++;
                start = i;
            }
        }
        if(j != needle.length()){
            return -1;
        }
        return index;
    }
}
