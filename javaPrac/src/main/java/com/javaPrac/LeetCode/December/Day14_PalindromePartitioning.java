package com.javaPrac.LeetCode.December;

import java.util.ArrayList;
import java.util.List;

public class Day14_PalindromePartitioning {
    public static void main(String[] args) {
        partition("aab");
    }
    public static List<List<String>> partition(String s) {
        List<List<String>> result = new ArrayList<>();
        if(s == null || s.length()==0){
            return result;
        }
        List<String> palindromeStrings = new ArrayList<>();
        addPalindrome(s, 0, palindromeStrings, result);
        return result;
    }

    private static void addPalindrome(String s, int start, List<String> palindromeStrings, List<List<String>> result) {
        if(start==s.length()){
            ArrayList<String> temp = new ArrayList<String>(palindromeStrings);
            result.add(temp);
            return;
        }
        for (int i = start + 1; i <= s.length(); i++) {
            String str = s.substring(start, i);
            if (isPalindrome(str)) {
                palindromeStrings.add(str);
                addPalindrome(s, i, palindromeStrings, result);
                palindromeStrings.remove(palindromeStrings.size() - 1);
            }
        }
    }

    private static boolean isPalindrome(String s){
        StringBuffer buffer = new StringBuffer(s);
        if(buffer.reverse().equals(buffer)){
            return true;
        }
        return false;
    }
}
