package com.javaPrac.LeetCode.December;

public class Day16_ValidBST {
    static class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode() {}
      TreeNode(int val) { this.val = val; }
      TreeNode(int val, TreeNode left, TreeNode right) {
          this.val = val;
          this.left = left;
          this.right = right;
      }
  }
    public static boolean isValidBST(TreeNode root) {
        return validate(root, null, null);
    }

    public static boolean validate(TreeNode root, Integer max, Integer min){
        if(root == null){
            return true;
        }else if(max != null && root.val >= max || min != null && root.val <= min){
            return false;
        }else{
            return validate(root.left, root.val, min) && validate(root.right, max, root.val);
        }
    }

    public static void main(String[] args) {
/*        TreeNode node1 = new TreeNode(1);
        TreeNode node3 = new TreeNode(3);
        TreeNode node4 = new TreeNode(6);
        TreeNode node2 = new TreeNode(4, node3, node4);
        TreeNode root = new TreeNode(5, node1, node2);
        System.out.print(isValidBST(root));*/

        /*TreeNode node11 = new TreeNode(1);
        TreeNode node22 = new TreeNode(3);
        TreeNode roott = new TreeNode(2, node11, node22);
        System.out.print(isValidBST(roott));*/

        TreeNode node1 = new TreeNode(3);
        TreeNode node3 = new TreeNode(7);
        TreeNode node4 = new TreeNode(4);
        TreeNode node2 = new TreeNode(6, node1, node3);
        TreeNode root = new TreeNode(5, node4, node2);
        System.out.print(isValidBST(root));
    }
}
