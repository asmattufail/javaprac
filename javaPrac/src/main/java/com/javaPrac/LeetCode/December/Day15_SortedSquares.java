package com.javaPrac.LeetCode.December;

import java.util.Arrays;

public class Day15_SortedSquares {

    public static void main(String args[]){
        int[] nums = new int[]{-4,-1,0,3,10};
        sortedSquares(nums);
        Arrays.stream(nums).boxed().forEach(x->System.out.println(x));
    }

    public static int[] sortedSquares(int[] nums) {
        int length = nums.length;
        for(int i = 0; i < length; i++){
            nums[i] = nums[i]*nums[i];
        }
        Arrays.sort(nums);
        return nums;
    }
}
