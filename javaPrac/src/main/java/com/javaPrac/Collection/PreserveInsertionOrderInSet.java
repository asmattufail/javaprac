package com.javaPrac.Collection;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Set;

public class PreserveInsertionOrderInSet {

    public static void main(String args[]){
    Set<String> set = Collections.newSetFromMap(new LinkedHashMap<>(16, 0.75f, true));
        set.add("one");
        set.add("two");
        set.add("three");
        set.add("two");
        System.out.println(set);

    }


}
