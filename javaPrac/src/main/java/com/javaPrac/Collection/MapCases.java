package com.javaPrac.Collection;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

public class MapCases {

    public static void main(String args[]){
        Map<Integer,String> map = new HashMap<>();
        map.put(1,"Asmat");
        map.put(2,"Komal");
        map.put(3,"Arshi");
        map.put(4,"Komal");
        map.put(5,"Jansi");
        //replaceKeyWithValues(map);
        //getKeysBasedOnValue(map, "Komal");
        int[] nums = new int[]{8,1,2,2,3};
        smallerNumbersThanCurrent(nums);
    }

    private static void replaceKeyWithValues(Map<Integer, String> map) {
        Map<String, List<Integer>> result =  map.entrySet().stream()
                .collect(groupingBy(Map.Entry::getValue,mapping(Map.Entry::getKey, toList())));
        result.entrySet().forEach(entry->System.out.println(entry.getKey()+" "+entry.getValue()));
    }

    private static void getKeysBasedOnValue(Map<Integer, String> map, String value) {
        List<Integer> keyList = new ArrayList<>();
        map.forEach((k,v)->{
            if(v.equals(value)){
                keyList.add(k);
            }
        });
        keyList.forEach(key -> System.out.println(key));
    }

    public static int[] smallerNumbersThanCurrent(int[] nums) {
        int result[] = new int[nums.length];
        int temp[] = Arrays.copyOf(nums,nums.length);
        Arrays.sort(temp);
        Map<Integer,Long> countMap = Arrays.stream(nums).boxed().collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
        Map<Integer,Integer> resultMap = new HashMap<>();
        for(int i = temp.length-1; i >= 0; i--){
            if(!resultMap.containsKey(temp[i]))
                resultMap.put(temp[i],i - (countMap.get(temp[i]).intValue())+1);
        }
        for(int j =0; j<nums.length; j++){
            result[j] = resultMap.get(nums[j]);
        }

        return result;
    }

    public int[] decompressRLElist(int[] nums) {
        int[] result = new int[]{};

        /*for(int i = 0; i <= nums.length ; i+2){

        }*/
        return result;
    }

}
